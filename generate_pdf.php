<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
        <link rel="stylesheet" href="css/signature_pdf.css">
	</head>
	<body>
		<div id="signArea" >
			<form method="post" action="print.php">
                <div class="sign-container">
                <h2>Thanks</h2>
                <input type="hidden" name="image" value="<?php echo $_GET['image']; ?>">
                <img src="<?php echo $_GET['image']; ?>" class="sign-preview" />
                </div>
                <button name="btnSaveSign" id="btnSaveSign">Download Pdf</button>
            </form>
        </div>
        <script src="js/app.js"></script>
	</body>
</html>